FROM python:2.7-slim
MAINTAINER attakei

ARG deps=""
ARG buildDeps="gcc libssl-dev"

# Install pip packages
ADD ./requirements.txt /tmp/requirements.txt
RUN set -x \
    && apt-get update \
    && apt-get install -y $deps $buildDeps --no-install-recommends \
    && rm -rf /var/lib/apt/lists/* \
    && pip install -r /tmp/requirements.txt \
    && apt-get purge -y --auto-remove $buildDeps \
    && sleep 0

RUN mkdir /app
WORKDIR /app
VOLUME /app

